/**
 * @file Handle server stores
 * @name server_stores.ts
 * @license MIT
 */

import S3 from "@aws-sdk/client-s3";

import CommunicationDB from "./db/CommunicationDB";
import AuthDB from "./db/AuthDB";

import { Config } from "./db/helpers/SQL";

// ...
export const comms = new CommunicationDB();
export const auth = new AuthDB();

// ...s3
if (!Config.s3_endpoint) throw new Error("Cannot start: missing S3 endpoint!");
export const S3Client = new S3.S3Client({
    region: Config.s3_region,
    endpoint: Config.s3_endpoint,
    credentials: {
        accessKeyId: Config.s3_accessKeyId as string,
        secretAccessKey: Config.s3_secretAccessKey as string,
    },
});

// default export
export default {
    comms,
    auth,
    S3Client,
};
