import type { RequestHandler } from "./$types";
import { json, error } from "@sveltejs/kit";

import { MINIMUM_ACTION_REQUIREMENTS, type Message } from "$lib/db/CommunicationDB";
import { VerifyContentType } from "$lib/db/helpers/RequestUtilities";
import { ComputeRandomObjectHash } from "$lib/db/helpers/Hash";
import { comms, auth } from "$lib/server_stores";

export const POST: RequestHandler = async (props) => {
    // verify content type
    const WrongType = VerifyContentType(props.request, "multipart/form-data");
    if (WrongType) return WrongType;

    // get userid from cookie
    const cookie = props.cookies.get("__Secure-StarID");

    // check expected values
    if (!cookie)
        return json(
            {
                success: false,
                message: "Missing cookie",
            },
            { status: 400 }
        );

    // get user
    const user = await auth.GetUserFromID(cookie);
    if (!user[0] || !user[2]) error(401);

    // check user membership in galaxy
    const membership = await comms.GetUserGroupMembership(
        user[2].Username,
        props.params.id
    );

    if (!membership[0] || membership[2] < MINIMUM_ACTION_REQUIREMENTS.SEND_MESSAGES)
        return error(401, "Unauthorized"); // user MUST be AT LEAST a member

    // get body
    const body = await props.request.formData();

    // check expected values
    if (!body.get("Content") && !body.get("attachments"))
        return json(
            {
                success: false,
                message: "Missing body.Content",
            },
            { status: 400 }
        );

    // get current galaxy
    const galaxy = await comms.GetGroupByID(props.params.id);
    if (!galaxy[0] || !galaxy[2]) return error(404, galaxy[1]);

    // get current channel
    const channel = await comms.GetChannelByID(props.params.channel);
    if (!channel[0] || !channel[2]) return error(404, channel[1]);

    // upload attachments
    const FileList: string[] = [];
    if (body.get("attachments") && body.getAll("attachments").length > 0)
        for (const attachment of body.getAll("attachments")) {
            if ((attachment as File).size === 0) continue;
            const attachment_id = ComputeRandomObjectHash();
            FileList.push(attachment_id);

            // remove await to speed up message uploading at the cost of
            // images not being loaded when the message is rendered on client
            const res = await comms.UploadAttachment(
                channel[2].ID,
                attachment as File,
                attachment_id
            );

            if (typeof res[0] === "boolean")
                return json(
                    {
                        success: res[0],
                        message: res[1],
                        payload: undefined,
                    },
                    {
                        status: 400,
                    }
                );
        }
    // create message and return
    const res = await comms.CreateMessage({
        Content: (body.get("Content") as string) || ":image_upload",
        Channel: channel[2].ID,
        Reply: (body.get("Reply") as any) || "",
        Author: user[2].Username,
        // these values will be filled automatically
        ID: "",
        Created: 0,
        Edited: 0,
        $metadata: {
            Attachments: FileList || [],
        } as Message["$metadata"],
    });

    return json(
        {
            success: res[0],
            message: res[1],
            payload: res[2],
        },
        {
            status: res[0] === true ? 200 : 400,
        }
    );
};

export const GET: RequestHandler = async (props) => {
    // get current galaxy
    const galaxy = await comms.GetGroupByID(props.params.id);
    if (!galaxy[0] || !galaxy[2]) return error(404, galaxy[1]);

    // get current channel
    const channel = await comms.GetChannelByID(props.params.channel);
    if (!channel[0] || !channel[2]) return error(404, channel[1]);

    // get messages and return
    const res = await comms.GetChannelMessages(channel[2].ID);

    return json(
        {
            success: res[0],
            message: res[1],
            payload: res[2],
        },
        {
            status: res[0] === true ? 200 : 400,
        }
    );
};
