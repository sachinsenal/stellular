import type { RequestHandler } from "./$types";
import { json, error } from "@sveltejs/kit";

import { MINIMUM_ACTION_REQUIREMENTS } from "$lib/db/CommunicationDB";
import { VerifyContentType } from "$lib/db/helpers/RequestUtilities";
import { comms, auth } from "$lib/server_stores";

export const PUT: RequestHandler = async (props) => {
    // verify content type
    const WrongType = VerifyContentType(props.request, "application/json");
    if (WrongType) return WrongType;

    // get userid from cookie
    const cookie = props.cookies.get("__Secure-StarID");

    // check expected values
    if (!cookie)
        return json(
            {
                success: false,
                message: "Missing cookie",
            },
            { status: 400 }
        );

    // get user
    const user = await auth.GetUserFromID(cookie);
    if (!user[0] || !user[2]) error(401, user[1]);

    // get body
    const body = await props.request.json();

    // check expected values
    if (!body.Content)
        return json(
            {
                success: false,
                message: "Missing body.Content",
            },
            { status: 400 }
        );

    // get message
    const message = await comms.GetMessageByID(props.params.message);
    if (!message[0] || !message[2]) error(401, message[1]);

    // make sure user is message author
    if (user[2].Username !== message[2].Author) return error(401, "Unauthorized");
    // get current galaxy
    const galaxy = await comms.GetGroupByID(props.params.id);
    if (!galaxy[0] || !galaxy[2]) return error(404, galaxy[1]);

    // get current channel
    const channel = await comms.GetChannelByID(props.params.channel);
    if (!channel[0] || !channel[2]) return error(404, channel[1]);

    // edit message and return
    const res = await comms.EditMessage(props.params.message, body.Content);

    return json(
        {
            success: res[0],
            message: res[1],
            payload: res[2],
        },
        {
            status: res[0] === true ? 200 : 400,
        }
    );
};

export const GET: RequestHandler = async (props) => {
    // get current galaxy
    const galaxy = await comms.GetGroupByID(props.params.id);
    if (!galaxy[0] || !galaxy[2]) return error(404, galaxy[1]);

    // get current channel
    const channel = await comms.GetChannelByID(props.params.channel);
    if (!channel[0] || !channel[2]) return error(404, channel[1]);

    // get message and return
    const res = await comms.GetMessageByID(props.params.message);

    return json(
        {
            success: res[0],
            message: res[1],
            payload: res[2],
        },
        {
            status: res[0] === true ? 200 : 400,
        }
    );
};

export const DELETE: RequestHandler = async (props) => {
    // get userid from cookie
    const cookie = props.cookies.get("__Secure-StarID");

    // check expected values
    if (!cookie)
        return json(
            {
                success: false,
                message: "Missing cookie",
            },
            { status: 400 }
        );

    // get user
    const user = await auth.GetUserFromID(cookie);
    if (!user[0] || !user[2]) error(401, user[1]);

    // get message
    const message = await comms.GetMessageByID(props.params.message);
    if (!message[0] || !message[2]) error(401, message[1]);

    // check user membership in galaxy
    const membership = await comms.GetUserGroupMembership(
        user[2].Username,
        props.params.id
    );

    if (
        !membership[0] ||
        (membership[2] < MINIMUM_ACTION_REQUIREMENTS.MANAGE_MESSAGES &&
            user[2].Username !== message[2].Author)
    )
        return error(401, "Unauthorized"); // user MUST be AT LEAST an owner OR message author

    // get current galaxy
    const galaxy = await comms.GetGroupByID(props.params.id);
    if (!galaxy[0] || !galaxy[2]) return error(404, galaxy[1]);

    // get current channel
    const channel = await comms.GetChannelByID(props.params.channel);
    if (!channel[0] || !channel[2]) return error(404, channel[1]);

    // delete message and return
    const res = await comms.DeleteMessage(props.params.message);

    return json(
        {
            success: res[0],
            message: res[1],
            payload: res[2],
        },
        {
            status: res[0] === true ? 200 : 400,
        }
    );
};
