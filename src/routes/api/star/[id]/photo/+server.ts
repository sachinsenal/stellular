import type { RequestHandler } from "./$types";
import { json, error } from "@sveltejs/kit";

import { VerifyContentType } from "$lib/db/helpers/RequestUtilities";
import { auth } from "$lib/server_stores";

export const POST: RequestHandler = async (props) => {
    // verify content type
    const WrongType = VerifyContentType(props.request, "multipart/form-data");
    if (WrongType) return WrongType;

    // get userid from cookie
    const cookie = props.cookies.get("__Secure-StarID");

    // check expected values
    if (!cookie)
        return json(
            {
                success: false,
                message: "Missing cookie",
            },
            { status: 400 }
        );

    // get user
    const user = await auth.GetUserFromID(cookie);
    if (!user[0] || !user[2]) error(401, user[1]);

    // get requested user
    const requested_user = await auth.GetUserFromID(props.params.id);
    if (!requested_user[0] || !requested_user[2]) error(404, requested_user[1]);

    // users can only update themselves
    if (user[2].ID !== requested_user[2].ID) return error(401, "Unauthorized");

    // get body
    const body = await props.request.formData();

    // check expected values
    if (!body.get("photo"))
        return json(
            {
                success: false,
                message: "Missing body.photo",
            },
            { status: 400 }
        );

    // update requested_user and return
    const res = await auth.UploadProfilePicture(
        requested_user[2].Username,
        body.get("photo") as File
    );

    if (typeof res[0] === "boolean")
        return json(
            {
                success: res[0],
                message: res[1],
                payload: undefined,
            },
            {
                status: 400,
            }
        );

    return json(
        {
            success: true,
            message: "",
            payload: "",
        },
        {
            status: 200,
        }
    );
};
