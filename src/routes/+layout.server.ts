import type { LayoutServerLoad } from "./$types";
import { error } from "@sveltejs/kit";

import { Config } from "$lib/db/helpers/SQL";
import { auth } from "$lib/server_stores";

export const load: LayoutServerLoad = async ({ cookies, url }) => {
    let data: { [key: string]: any } = {};

    // get auth cookie
    const cookie = cookies.get("__Secure-StarID");

    if (cookie) {
        // get user
        const user = await auth.GetUserFromID(cookie);
        if (!user[0] || !user[2]) return error(401, "Session Invalid");

        // return
        data.user = user[2];
    }

    // ...
    data.Config = { s3_public_endpoint: Config.s3_public_endpoint };

    // galaxy view
    if (url.pathname.startsWith("/dashboard/galaxy/")) data.galaxy_view = true;

    // return
    return data;
};
