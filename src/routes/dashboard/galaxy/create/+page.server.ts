import type { PageServerLoad } from "./$types";
import { error } from "@sveltejs/kit";

import { comms, auth } from "$lib/server_stores";

export const load: PageServerLoad = async ({ cookies }) => {
    let data: { [key: string]: any } = {};

    // get auth cookie
    const cookie = cookies.get("__Secure-StarID");

    if (cookie) {
        // get user
        const user = await auth.GetUserFromID(cookie);
        if (!user[0] || !user[2]) return error(401, "Session Invalid");
        data.user = user[2];
    } else return error(401, "Unauthorized");

    // return
    return data;
};
